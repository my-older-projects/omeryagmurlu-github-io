---
layout: page
title: Hakkımda
permalink: /hakkimda/
---

Adım Ömer Yağmurlu, 2000 yılında doğdum. İstanbul Erkek Lisesinde okuyorum, (ve bundan gurur duyuyorum). 5-6 senedir tasarıma ve programlamaya özel ilgim var, web dillerini bir ayrı seviyorum. C, Lua, JS, Phyton rahat bir şekilde kullanıyorum. CSS3 ve ES6'in tüm özelliklerini biliyor ve şu sıralarda da CSS4 draftları ve ESNEXT standartlarını öğreniyorum. 2014 Teogdan tam puan alarak ülkemin en iyi lisesi olan İEL'e girdim. 2019 yılında mezun olup inş Abitur ve ÖSS yapmış olacağım. Almanca İngilizce biliyorum, okuduğum kaynakların çoğu İngilizce olduğundan, İngilizce Programlama dilini de biliyorum.

#### Hobilerim

Hobi olarak eğer zamanım bolsa Video oyun yazıyorum (2d :D), kısıtlıysa ya oyun oynuyorum, ya küçük Html/CSS/JS oyun/site/tasarıları, yada şu an yaptığım gibi aklıma ne gelirse yapıyorum. Küçük, hayat kolaylaştırıcı scriptler yazıyorum, genelde bash'ta başlayan kodlar çoğunlukla nod.js'de yada python'da bitiyor. Çok kullandığım bazı web servislerini otomatikleştirmek için onları `scrape`leyip bir nevi API'lar haline getiriyorum. Gtk veya Qt öğrenmeye üşendiğimden herşeyi Electron'da (eskinde nw.js'de) yapıyorum. Ayrıca [GNOME kullanmama rağmen laptopumu `rice` etmeyi seviyorum](https://www.reddit.com/r/unixporn/comments/539fj1/gnome_gnome_can_rice_too/). Arduino ile robot yapmayı da seviyorum.

En sevdiğim renkler ise gri ve yeşil tonları.
