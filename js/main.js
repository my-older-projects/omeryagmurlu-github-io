$(function() {
	$("html").addClass("new");
	setTimeout(function(){
		$("html").removeClass("new");
	},4500);


	/**
	* Eventers
	*/
	$("body")
	.on("mouseup", function(){
		$("body *").removeClass("mousedown");
	})
	.on("mousedown", function(){
		$("body *").removeClass("clicked");
	});

	$("body *")
	.on("mousedown", function(){
		$(this).addClass("mousedown");
	})
	.on("mouseup", function(){
		$(this).addClass("clicked");
	});

	$("body *").mouseenter( function() { $(this).addClass("hover"); } ).mouseleave( function() { $(this).removeClass("hover"); } );

	/**
	* Scroll
	*/
	var html = $("html");
	var lastScrollTop = 0;
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		
		if (scroll <= 0) {
			html.removeClass("notAtTheBeginning");
		} else {
			html.addClass("notAtTheBeginning");
		}

		if (scroll > lastScrollTop){
			html.addClass("down").removeClass("up")
		} else {
			html.addClass("up").removeClass("down");
		}
		lastScrollTop = scroll;

		if(scroll + $(window).height() >= $(document).height()){
			html.addClass("atTheEnd").addClass("reachedTheEnd");
		} else {
			html.removeClass("atTheEnd");
		}
	});

	/**
	* 100vh
	*/
	$(".yuzvh").css("height",window.innerHeight);
});

