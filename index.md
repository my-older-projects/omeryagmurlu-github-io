---
layout: page
custom_css:
- https://fonts.googleapis.com/css?family=Freckle+Face&amp;subset=latin,latin-ext
image: "url(/assets/img/green.jpg); font-family: 'Freckle Face', cursive"
permalink: /index.html
---

## Merhaba
Fark edebileceğiniz gibi, benim adım Ömer Yağmurlu ve burası da benim sitem. Burada daha sonra benim `resume´im olacak büyük ihtimalle, şimdilik burası böyle olcak.

Ayrıca hobi olarak Node.js ile büyüklüğü değişebilen ama genellikle kısa ve öz, trivial kodlar yazıyorum, hobi olarak, ciddi bir iş değil yani. Ama her ne kadar hobi olarak yapsamda benim de kendime göre tecrübelerim, prensiplerim var. Onları da ayrıntılı olarak bloğumda anlatacağım.

<br>

<article>
	{% assign post = site.posts.first %}
	<h3>En son yazı</h3>
	<h5><a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a></h5>
	<p>
		<time>{{ post.date | date_to_string }}</time> {% if post.author %} • <a href="{% if post.author == 'omer' %} /hakkimda/ {% else %} /authors#{{ post.author }} {% endif %} " class="author">{{ site.authors[post.author].display_name }}</a> {% endif %}
	</p>
	<p>{{ post.content | strip_html | truncatewords:15}}</p>
	<a href="{{ post.url }}">Devamı</a>

</article>
